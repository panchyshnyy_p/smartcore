/**
 * Smart Home page module
 * Adminhtml javascript handler
 *
 * @category    Smart
 * @package     Smart_Homepage
 * @author      Pavel Panchyshnyy ppanchyshnyy@gmail.com
 */

var SmartHandler = Class.create();

SmartHandler.prototype = {
    initialize: function () {
        this.buttons = '.add-widget-button';
        this.observeWidgets();
        this.observeRowsClick();
    },

    observeWidgets: function () {
        var handler = this;

        $$(this.buttons).invoke('observe', 'click', function () {
            handler._onWidgetClick(this);
        });
    },

    observeRowsClick: function () {
        var handler = this;

        $$('.redirect-title').invoke('observe', 'click', function () {
            handler._onRowClick(this);
        });
    },

    /**
     * Redirects to New Widget form
     *
     * @param element
     */
    _onWidgetClick: function (element) {
        var redirect = element.readAttribute('data-redirect-url');

        if(redirect.length){
            setLocation(redirect);
        }
    },

    _onRowClick: function (element) {
        var redirect = element.readAttribute('title');

        if(redirect.length){
            setLocation(redirect);
        }
    }
}