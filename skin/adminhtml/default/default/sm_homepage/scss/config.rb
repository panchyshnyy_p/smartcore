# note: this should never truly be referenced since we are using relative assets
http_path = "/skin/adminhtml/default/default/"
css_dir = "../css"
sass_dir = "../scss"
images_dir = "../images"
relative_assets = true

output_style = :compressed
environment = :development