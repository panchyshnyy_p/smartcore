<?php

class HpsAddress
{
    public $address  = null;
    public $address2 = null;
    public $city     = null;
    public $state    = null;
    public $zip      = null;
    public $country  = null;
}
