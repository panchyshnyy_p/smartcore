<?php
/**
 * Smart Home page module
 * Banner's main Model
 *
 * @category    Smart
 * @package     Smart_Homepage
 * @author      Pavel Panchyshnyy ppanchyshnyy@gmail.com
 */

class Smart_Homepage_Model_Banner extends Mage_Core_Model_Abstract
{
    /**
     * Banner's Statuses
     */
    const STATUS_ENABLED  = 1;
    const STATUS_DISABLED = 0;

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'sm_homepage_banner';

    /**
     * Initialize resource model
     *
     */
    protected function _construct()
    {
        $this->_init('sm_homepage/banner');
    }

    /**
     * @return Mage_Core_Model_Abstract
     */
    protected function _beforeSave()
    {
        if (is_array($this->getData('image'))) {
            $this->unsetData('image');
        }

        return parent::_beforeSave();
    }

    /**
     * Prepare Banner's statuses.
     * Available event sm_homepage_banner_get_available_statuses to customize statuses.
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        $statuses = new Varien_Object(array(
            self::STATUS_ENABLED  => Mage::helper('sm_homepage')->__('Enabled'),
            self::STATUS_DISABLED => Mage::helper('sm_homepage')->__('Disabled'),
        ));

        Mage::dispatchEvent('sm_homepage_banner_get_available_statuses', array('statuses' => $statuses));

        return $statuses->getData();
    }
}