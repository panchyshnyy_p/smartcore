<?php
/**
 * Smart Home page module
 * Main data helper
 *
 * @category    Smart
 * @package     Smart_Homepage
 * @author      Pavel Panchyshnyy ppanchyshnyy@gmail.com
 */

class Smart_Homepage_Model_Widget_Type extends Varien_Object
{
    const WIDGET_TYPE_SLIDER = 'slider';
    const WIDGET_TYPE_VIDEO  = 'video';
    const WIDGET_TYPE_BANNER = 'banner';

    /**
     * @return array
     */
    static public function getTypes()
    {
        $helper  = Mage::helper('sm_homepage');

        return array(
            self::WIDGET_TYPE_BANNER => $helper->__('Banner'),
            self::WIDGET_TYPE_SLIDER => $helper->__('Slider'),
            self::WIDGET_TYPE_VIDEO  => $helper->__('Video')
        );
    }

    /**
     * @return array
     */
    public function getOptionsArray()
    {
        $options = array();
        $helper  = Mage::helper('sm_homepage');
        $types   = self::getTypes();

        foreach($types as $type => $value){
            $options[] = array(
                'index' => $type,
                'value' => $value
            );
        }

        array_unshift($options, array('index' => '', 'value' => $helper->__('Select Widget type')));

        return $options;
    }
}