<?php
/**
 * Smart Home page module
 * Main Handler-Observer
 *
 * @category    Smart
 * @package     Smart_Homepage
 * @author      Pavel Panchyshnyy ppanchyshnyy@gmail.com
 */

class Smart_Homepage_Model_Observer
{
    /**
     * Adding Smart Homepage content like Banners, Sliders, Videos before everything
     *
     * @event 'cms_page_render'
     * @param Varien_Event_Observer $observer
     * @return $this|void
     */
    public function beforeCmsPageRender(Varien_Event_Observer $observer)
    {
        $controller = $observer->getControllerAction();
        $isHomePage = $controller->getLayout()->getBlockSingleton('page/html_header')->getIsHomePage();

        if($isHomePage){
            $layout = $controller->getLayout()->getUpdate();
            $layout->addHandle('smart_homepage');

            return $this;
        }

        return;
    }
}