<?php
/**
 * Smart Home page module
 * Banner Adminhtml controller
 *
 * @category    Smart
 * @package     Smart_Homepage
 * @author      Pavel Panchyshnyy ppanchyshnyy@gmail.com
 */

class Smart_Homepage_Adminhtml_BannerController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Load layout, set active menu and breadcrumbs
     *
     * @return $this
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('sm_homepage')
            ->_addBreadcrumb($this->__('Smart Home Page'), $this->__('Smart Home Page'))
            ->_addBreadcrumb($this->__('Home Page settings'), $this->__('Home Page settings'));

        return $this;
    }

    /**
     * Create new Banner
     */
    public function newAction()
    {
        // the same form is used to create and edit
        $this->_forward('edit');
    }

    /**
     * Edit existing Banner
     */
    public function editAction()
    {
        $this->_title($this->__('Manage Home Page'))->_title($this->__('Banners'));

        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('banner_id');
        $model = Mage::getModel('sm_homepage/banner');

        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (! $model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('sm_homepage')->__('This Banner no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }
        }

        $this->_title($model->getId() ? $model->getTitle() : $this->__('New Banner'));

        // 3. Set entered data if was error when we do save
        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if (! empty($data)) {
            $model->setData($data);
        }

        // 4. Register model to use later in blocks
        Mage::register('sm_homepage_banner', $model);

        // 5. Build edit form
        $this->_initAction()
            ->_addBreadcrumb($id ? Mage::helper('sm_homepage')->__('Edit Banner') : Mage::helper('sm_homepage')->__('New Banner'),
                $id ? Mage::helper('sm_homepage')->__('Edit Banner') : Mage::helper('sm_homepage')->__('New Banner'))
            ->renderLayout();
    }

    /**
     * Save Banner action
     */
    public function saveAction()
    {
        // check if data sent
        if ($data = $this->getRequest()->getPost()) {

            $id = $this->getRequest()->getParam('banner_id');
            $model  = Mage::getModel('sm_homepage/banner')->load($id);
            $helper = Mage::helper('sm_homepage');

            if (!$model->getId() && $id) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('sm_homepage')->__('This Banner no longer exists.'));
                $this->_redirect('adminhtml/homepage/index');
                return;
            }
            // init model and set data
            $model->setData($data);

            // try to save it
            try {
                $helper->uploadImage($model, $data, 'banner');
                $model->save();

                // display success message
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('sm_homepage')->__('The Banner %s has been saved.', $model->getTitle()));
                // clear previously saved data from session
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                // check if 'Save and Continue'
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('banner_id' => $model->getId()));
                    return;
                }
                // go to settings
                $this->_redirect('adminhtml/homepage/index');

                return;
            } catch (Exception $e) {
                // display error message
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                // save data in session
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                // redirect to edit form
                $this->_redirect('*/*/edit', array('banner_id' => $this->getRequest()->getParam('banner_id')));
                return;
            }
        }
        $this->_redirect('adminhtml/homepage/index');
    }

    /**
     * Delete Banner action
     */
    public function deleteAction()
    {
        // check if we know what should be deleted
        if ($id = $this->getRequest()->getParam('banner_id')) {
            $title = "";
            try {
                // init model and delete
                $model = Mage::getModel('sm_homepage/banner');
                $model->load($id);
                $title = $model->getTitle();
                $model->delete();
                // display success message
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('sm_homepage')->__('The Banner %s has been deleted.', $title));
                // go to grid
                $this->_redirect('adminhtml/homepage/index');
                return;

            } catch (Exception $e) {
                // display error message
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                // go back to edit form
                $this->_redirect('*/*/edit', array('banner_id' => $id));
                return;
            }
        }
        // display error message
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('sm_homepage')->__('Unable to find a Banner to delete.'));
        // go to settings
        $this->_redirect('adminhtml/homepage/index');
    }

    /**
     * Check the permission to run it
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('sm_homepage');
    }
}