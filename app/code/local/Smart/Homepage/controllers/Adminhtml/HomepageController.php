<?php
/**
 * Smart Home page module
 * Main Adminhtml controller
 *
 * @category    Smart
 * @package     Smart_Homepage
 * @author      Pavel Panchyshnyy ppanchyshnyy@gmail.com
 */

class Smart_Homepage_Adminhtml_HomepageController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Load layout, set active menu and breadcrumbs
     *
     * @return $this
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('sm_homepage')
            ->_addBreadcrumb($this->__('Smart Home Page'), $this->__('Smart Home Page'))
            ->_addBreadcrumb($this->__('Home Page settings'), $this->__('Home Page settings'));

        return $this;
    }

    /**
     * Main wrapper for configuring Home Page
     *
     */
    public function indexAction()
    {
        $this->_title($this->__('Smart Home Page'))
            ->_title($this->__('Home Page settings'));

        $this->_initAction()->renderLayout();
    }

    /**
     * Ajax request action
     */
    public function handleWidgetsAction()
    {
        $type = $this->getRequest()->getParam('banner_id');

        $this->_initAction();
        $block = $this->getLayout()->createBlock('core/text')->setText("<h1>$type</h1>");
        $this->getLayout()->getBlock('content')->append($block);
        $this->renderLayout();
    }
}