<?php
/**
 * Smart Home page module
 * Main data helper
 *
 * @category    Smart
 * @package     Smart_Homepage
 * @author      Pavel Panchyshnyy ppanchyshnyy@gmail.com
 */

class Smart_Homepage_Helper_Data extends Mage_Core_Helper_Data
{
    const IMG_PATH_DIR_BANNERS = 'smart/homepage/banners';
    const IMG_PATH_DIR_SLIDERS = 'smart/homepage/sliders';
    const IMG_PATH_DIR_VIDEOS  = 'smart/homepage/videos';


    /**
     * @param $entity
     * @return string
     */
    protected function _getImagePath($entity)
    {
        $pathPrefix = '';

        switch ($entity) {
            case 'banner':
                $pathPrefix = self::IMG_PATH_DIR_BANNERS;
                break;
            case 'slider':
                $pathPrefix = self::IMG_PATH_DIR_SLIDERS;
                break;
            case 'video':
                $pathPrefix = self::IMG_PATH_DIR_VIDEOS;
                break;
        }

        return $pathPrefix;
    }

    /**
     * @param string $filename
     * @param $type
     * @return string
     */
    public function getImageUrl($filename = '', $type)
    {
        if ($filename) {
            $dir = $this->_getImagePath($type);
            $urlPrefix = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . $dir;

            return $urlPrefix . $filename;
        } else {
            return '';
        }
    }

    /**
     * @param $model
     * @param $data
     * @param $type
     * @throws Exception
     */
    public function uploadImage($model, $data, $type)
    {
        if(isset($_FILES['image']['name']) and (file_exists($_FILES['image']['tmp_name']))) {
            try {
                $imageName = $_FILES['image']['name'];
                $uploader = new Varien_File_Uploader('image');
                $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
                $uploader->setAllowRenameFiles(false);
                $uploader->setFilesDispersion(true);
                $dir  = $this->_getImagePath($type);
                $path = Mage::getBaseDir('media') .DS. $dir;
                $uploader->save($path, $imageName);
                $model->setImage($uploader->getUploadedFileName());
            }catch(Exception $e) {
                throw new Exception($e->getMessage());
            }
        }
        else {
            if (isset($data['image']['delete']) && $data['image']['delete'] == 1) {
                $model->setData('image', '');
                $path = str_replace(Mage::getBaseUrl('media'), Mage::getBaseDir('media') . DS, $data['image']['value']);
                @unlink($path);
            }
        }
    }
}