<?php
/**
 * Smart Home page module
 * Banner Adminhtml edit form
 *
 * @category    Smart
 * @package     Smart_Homepage
 * @author      Pavel Panchyshnyy ppanchyshnyy@gmail.com
 */

class Smart_Homepage_Block_Adminhtml_Banner_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Init form
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('block_form');
        $this->setTitle(Mage::helper('sm_homepage')->__('Banner Information'));
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $model = Mage::registry('sm_homepage_banner');
        $formAction = Mage::getModel('adminhtml/url')->getUrl('adminhtml/banner/save', array('banner_id' => $model->getId()));

        $form = new Varien_Data_Form(
            array('id' => 'edit_form', 'action' => $formAction, 'method' => 'post', 'enctype' => 'multipart/form-data')
        );

        $form->setHtmlIdPrefix('sm_banner_');

        $fieldset = $form->addFieldset('base_fieldset', array(
            'legend' => Mage::helper('sm_homepage')->__('General Information'),
            'class' => 'fieldset-wide')
        );

        if ($model->getBannerId()) {
            $fieldset->addField('banner_id', 'hidden', array(
                'name' => 'banner_id',
            ));
        }

        $fieldset->addField('title', 'text', array(
            'name'      => 'title',
            'label'     => Mage::helper('sm_homepage')->__('Banner\'s Title'),
            'title'     => Mage::helper('sm_homepage')->__('Banner\'s Title'),
            'required'  => true
        ));

        $fieldset->addType('required_image', 'Smart_Homepage_Block_Adminhtml_Form_Field_Image');
        $fieldset->addField('image', 'required_image', array(
            'label'     => Mage::helper('sm_homepage')->__('Background Image'),
            'name'      => 'image',
            'note'      => '(*.jpg, *.png, *.gif)',
            'required'  => true,
        ));

        $fieldset->addField('content', 'textarea', array(
            'name'      => 'content',
            'label'     => Mage::helper('sm_homepage')->__('Content'),
            'title'     => Mage::helper('sm_homepage')->__('Content'),
            'style'     => 'height:36em',
            'required'  => true,
        ));

        $dateFormatIso = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
        $fieldset->addField('from_date', 'date', array(
            'name'   => 'from_date',
            'label'  => Mage::helper('sm_homepage')->__('From Date'),
            'title'  => Mage::helper('sm_homepage')->__('From Date'),
            'image'  => $this->getSkinUrl('images/grid-cal.gif'),
            'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
            'format'       => $dateFormatIso
        ));
        $fieldset->addField('to_date', 'date', array(
            'name'   => 'to_date',
            'label'  => Mage::helper('sm_homepage')->__('To Date'),
            'title'  => Mage::helper('sm_homepage')->__('To Date'),
            'image'  => $this->getSkinUrl('images/grid-cal.gif'),
            'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
            'format'       => $dateFormatIso
        ));

        $fieldset->addField('is_active', 'select', array(
            'label'     => Mage::helper('sm_homepage')->__('Status'),
            'title'     => Mage::helper('sm_homepage')->__('Status'),
            'name'      => 'is_active',
            'required'  => true,
            'options'   => $model->getAvailableStatuses(),
        ));

        $formData = array_merge($model->getData(),
            array('image' => Mage::helper('sm_homepage')->getImageUrl((string)$model->getImage(), 'banner'))
        );
        $this->_addElementTypes($fieldset);
        $form->setValues($formData);
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

}