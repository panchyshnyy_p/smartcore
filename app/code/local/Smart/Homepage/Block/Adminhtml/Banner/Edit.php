<?php
/**
 * Smart Home page module
 * Banner Adminhtml container form
 *
 * @category    Smart
 * @package     Smart_Homepage
 * @author      Pavel Panchyshnyy ppanchyshnyy@gmail.com
 */

class Smart_Homepage_Block_Adminhtml_Banner_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId   = 'banner_id';
        $this->_blockGroup = 'sm_homepage';
        $this->_controller = 'adminhtml_banner';
        $location = Mage::getModel('adminhtml/url')->getUrl('adminhtml/homepage/index');

        parent::__construct();

        $this->_updateButton('save', 'label', Mage::helper('sm_homepage')->__('Save Banner'));
        $this->_updateButton('delete', 'label', Mage::helper('sm_homepage')->__('Delete Banner'));
        $this->_updateButton('back', 'label', Mage::helper('sm_homepage')->__('Back To Settings'));
        $this->_updateButton('back', 'onclick', 'setLocation(\''.$location.'\')');

        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save and Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('block_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'block_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'block_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * Get edit form container header text
     *
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('sm_homepage_banner')->getId()) {
            return Mage::helper('sm_homepage')->__("Edit Banner '%s'", $this->escapeHtml(Mage::registry('sm_homepage_banner')->getTitle()));
        }
        else {
            return Mage::helper('sm_homepage')->__('New Banner');
        }
    }

}
