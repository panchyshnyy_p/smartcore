<?php
/**
 * Smart Home page module
 * Adminhtml layout updater
 *
 * @category    Smart
 * @package     Smart_Homepage
 * @author      Pavel Panchyshnyy ppanchyshnyy@gmail.com
 */

class Smart_Homepage_Block_Adminhtml_Main extends Mage_Adminhtml_Block_Template
{
    /**
     * Retrieves url to ajax widget's refresh
     *
     * @return string
     */
    public function getWidgetHandleUrl()
    {
        return Mage::getModel($this->_getUrlModelClass())->getUrl('adminhtml/homepage/handleWidgets');
    }

    /**
     * @return mixed
     */
    public function getWidgetTypesOptions()
    {
        return Mage::getModel('sm_homepage/widget_type')->getOptionsArray();
    }

    /**
     * @param string $type
     * @return mixed
     */
    public function getAddWidgetUrl($type)
    {
        return Mage::getModel($this->_getUrlModelClass())->getUrl('adminhtml/'.$type.'/new');
    }

    /**
     * @return object
     */
    public function getBannersCollection()
    {
        $banners = Mage::getModel('sm_homepage/banner')->getResourceCollection();

        return $banners;
    }

    public function getBannerEditUrl($banner)
    {
        return Mage::getModel($this->_getUrlModelClass())->getUrl('adminhtml/banner/edit', array('banner_id' => $banner->getId()));
    }
}