<?php
/**
 * Smart Home page module
 * Custom renderer for required images
 *
 * @category    Smart
 * @package     Smart_Homepage
 * @author      Pavel Panchyshnyy ppanchyshnyy@gmail.com
 */

class Smart_Homepage_Block_Adminhtml_Form_Field_Image extends Varien_Data_Form_Element_Image
{
    /**
     * Return element html code
     *
     * @return string
     */
    public function getElementHtml()
    {
        $html = '';

        if ((string)$this->getValue()) {
            $url = $this->_getUrl();

            if( !preg_match("/^http\:\/\/|https\:\/\//", $url) ) {
                $url = Mage::getBaseUrl('media') . $url;
            }

            $html = '<a href="' . $url . '"'
                . ' onclick="imagePreview(\'' . $this->getHtmlId() . '_image\'); return false;">'
                . '<img src="' . $url . '" id="' . $this->getHtmlId() . '_image" title="' . $this->getValue() . '"'
                . ' alt="' . $this->getValue() . '" height="22" width="22" class="small-image-preview v-middle" />'
                . '</a> ';
            $this->setClass('input-file');
        } else{
            $this->setClass('input-file required-file');
        }

        $html .= Varien_Data_Form_Element_Abstract::getElementHtml();
        $html .= $this->_getDeleteCheckbox();

        return $html;
    }
}