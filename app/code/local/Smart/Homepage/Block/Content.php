<?php
/**
 * Smart Home page module
 * Main content block for Banners, Sliders and Videos
 *
 * @category    Smart
 * @package     Smart_Homepage
 * @author      Pavel Panchyshnyy ppanchyshnyy@gmail.com
 */

class Smart_Homepage_Block_Content extends Mage_Core_Block_Template
{
    // Banners collection
    protected $_banners;
    // Sliders collection
    protected $_sliders;
    // Videos collection
    protected $_videos;

    protected $_template = 'smart/homepage/content.phtml';


    /**
     * @return Mage_Core_Block_Abstract
     */
    protected function _beforeToHtml()
    {
        $banners = Mage::getModel('sm_homepage/banner')->getCollection()
            ->addFieldToFilter('is_active', Smart_Homepage_Model_Banner::STATUS_ENABLED);
        $this->_banners = $banners;

        return parent::_beforeToHtml();
    }

    /**
     * Get all Banners
     *
     * @return mixed
     */
    public function getBanners()
    {
        return $this->_banners;
    }

    /**
     * Get All Widgets
     *
     * @return Varien_Object
     */
    public function getAllWidgets()
    {
        $widgets = new Varien_Object(array(
            'banners' => $this->getBanners(),
//            'sliders' => $this->getSliders(),
//            'videos'  => $this->getVideos()
        ));

        return $widgets;
    }
}