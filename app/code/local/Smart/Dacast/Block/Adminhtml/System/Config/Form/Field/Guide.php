<?php
/**
 * Dacast integration module
 * Custom renderer
 *
 * @category    Smart
 * @package     Smart_Dacast
 * @author      Pavel Panchyshnyy ppanchyshnyy@gmail.com
 */

class Smart_Dacast_Block_Adminhtml_System_Config_Form_Field_Guide extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    /**
     * Unsetting default behavior of system/config raw
     *
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }

    /**
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $url = $this->helper('sm_dacast')->getDacastUrl();

        return '<div id="sm_dacast_guide"><span>'.$this->__('Haven\'t register at Dacast yet? Follow this').'
         <a href="'.$url.'" class="dacast-link" target="_blank">'.$this->__('link').'</a></span></div>';
    }
}