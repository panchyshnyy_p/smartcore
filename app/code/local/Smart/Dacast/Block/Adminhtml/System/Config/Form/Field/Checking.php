<?php
/**
 * Dacast integration module
 * Custom renderer
 *
 * @category    Smart
 * @package     Smart_Dacast
 * @author      Pavel Panchyshnyy ppanchyshnyy@gmail.com
 */

class Smart_Dacast_Block_Adminhtml_System_Config_Form_Field_Checking extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    /**
     * Unsetting default behavior of system/config raw
     *
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }

    /**
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $element->setValue(Mage::app()->loadCache('admin_dacast_checking'));
        $scrypt = $this->_getScript();

        return '<ul class="messages" id="dacast_checking_result" style="display: none"></ul>
        <a href="javascript:void(0)" class="dacast-link" onclick="tryConnect(); return;">' . $this->__('Check Connection') . '</a>' . $scrypt;
    }

    /**
     * Script for ajax request and testing connection with Dacast
     *
     * @return string
     */
    protected function _getScript()
    {
        $action = Mage::helper('adminhtml')->getUrl('adminhtml/main/check');

        return '<script>function tryConnect(){
            var key = $("sm_dacast_settings_api_key").getValue(),
                bid = $("sm_dacast_settings_api_bid").getValue();
            var resultHandler = $("dacast_checking_result");

            if((key == "") || (bid == "")){
                alert("' . $this->__('Please enter your API key for checking!') . '");
                return;
            }else {
                new Ajax.Request("' . $action . '",{
                method: "post",
                parameters: {api_key: key, bid: bid},
                  onSuccess: function(transport) {
                    var response = transport.responseJSON;
                    resultHandler.update(response.message);
                    resultHandler.show();
                  },
                  onFailure: function() { alert("' . $this->__('Something went wrong. Please refresh the page and try again.') . '"); }
                });
            }
        }
</script>';
    }
}