<?php
/**
 * Dacast integration module
 * Main adminhtml controller
 *
 * @category    Smart
 * @package     Smart_Dacast
 * @author      Pavel Panchyshnyy ppanchyshnyy@gmail.com
 */

class Smart_Dacast_Adminhtml_MainController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Checking API credentials and connection with Dacast
     */
    public function checkAction()
    {
        $apiKey = $this->getRequest()->getParam('api_key');
        $bid    = $this->getRequest()->getParam('bid');
        $result = array();

        if ( $apiKey && $bid ) {
            $url = "https://api.dacast.com/v2/account/profile/personal?apikey=$apiKey&_format=json";
            $curl = Mage::getModel('sm_dacast/api_call')->init($apiKey, $bid, $url);
            $curl->ApiRequestWithRawData($url);
            $json = json_decode($curl->getJson(), true);

            if(count($json) && !isset($json['message'])){
                $result['status']  = 1;
                $result['message'] = '<li class="success-msg">'.$this->__('Connection has been configured successfully.').'</li>';
            } else {
                $result['status']  = 0;
                $result['message'] = '<li class="error-msg">'.$json['message'].'</li>';
            }
        } else {
            $result['status']  = 0;
            $result['message'] = '<li class="error-msg">'.$this->__('Entered API credentials were not correct. Please try again.').'</li>';
        }

        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        return;
    }
}