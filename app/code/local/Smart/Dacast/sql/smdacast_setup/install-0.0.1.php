<?php
/**
 * Dacast integration module
 * Install catalog_product attribute for Virtual Products only
 *
 * @category    Smart
 * @package     Smart_Dacast
 * @author      Pavel Panchyshnyy ppanchyshnyy@gmail.com
 */

$installer = $this;
$setup     = new Mage_Eav_Model_Entity_Setup('core_setup');
$helper    = Mage::helper('sm_dacast');

$installer->startSetup();

$attrGroupName  = $helper->__('Dacast Livestream');
$entityTypeId   = $setup->getEntityTypeId('catalog_product');
$attributeSetId = $setup->getDefaultAttributeSetId($entityTypeId);

$setup->addAttributeGroup($entityTypeId, $attributeSetId, $attrGroupName, 50);

$setup->addAttribute($entityTypeId, 'smdacast_stream', array(
    'group'                      => $attrGroupName,
    'input'                      => 'text',
    'type'                       => 'varchar',
    'label'                      => $helper->__('Livestream Video'),
    'backend'                    => '',
    'visible'                    => true,
    'required'                   => false,
    'sort'                       => 10,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE,
    'user_defined'               => false,
    'searchable'                 => false,
    'filterable'                 => false,
    'comparable'                 => false,
    'visible_on_front'           => true,
    'visible_in_advanced_search' => false,
    'is_html_allowed_on_front'   => false,
    'is_configurable'            => false,
));

$setup->updateAttribute($entityTypeId, 'smdacast_stream', 'apply_to', Mage_Catalog_Model_Product_Type::TYPE_VIRTUAL);

$installer->endSetup();