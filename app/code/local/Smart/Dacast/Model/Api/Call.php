<?php

/**
 * Dacast integration module
 * This class is use to make all the  API call, all the curl request are make here
 *
 * @category    Smart
 * @package     Smart_Dacast
 * @author      Pavel Panchyshnyy ppanchyshnyy@gmail.com
 */
class Smart_Dacast_Model_Api_Call extends Varien_Object
{
    protected $_log;
    protected $_logError;
    protected $_helper;

    /**
     * Initialization
     */
    protected function _construct()
    {
        $this->_helper   = Mage::helper('sm_dacast');
        $this->_log      = Mage::helper('sm_dacast/klogger')->init('APICall', Smart_Dacast_Helper_KLogger::INFO);
        $this->_logError = Mage::helper('sm_dacast/klogger')->init('error', Smart_Dacast_Helper_KLogger::ERR);
    }

    /**
     * @return mixed
     */
    protected function _getLogErrorHandler()
    {
        return $this->_logError;
    }

    /**
     * @return mixed
     */
    protected function _getLogHandler()
    {
        return $this->_log;
    }

    /**
     * @return mixed
     */
    protected function _getHelperInstance()
    {
        return $this->_helper;
    }

    /**
     * @param $apiKey
     * @param $broadcasterId
     * @param $url
     * @return $this
     * @throws Exception
     */
    public function init($apiKey, $broadcasterId, $url)
    {
        $errorHandler = $this->_getLogErrorHandler();
        $isLogEnabled = $this->_getHelperInstance()->isModuleEnabled();

        if (isset($apiKey)) {
            if (isset($broadcasterId)) {
                if (isset($url)) {
                    $this->setData(array(
                        'api_key'        => $apiKey,
                        'broadcaster_id' => $broadcasterId,
                        'url'            => $url
                    ));
                } else {
                    $isLogEnabled ? $errorHandler->logError(__LINE__ . ' ' . __FILE__ . $errorHandler->__(' - URL miss initialized')) : false;
                    throw new Exception($errorHandler->__('URL miss initialized in APICall constructor.'));
                }
            } else {
                $isLogEnabled ? $errorHandler->logError(__LINE__ . ' ' . __FILE__ . $errorHandler->__(' - Broadcaster_id miss initialized in APICall constructor')) : false;
                throw new Exception($errorHandler->__('Broadcaster_id miss initialized in APICall constructor.'));
            }
        } else {
            $isLogEnabled ? $errorHandler->logError(__LINE__ . ' ' . __FILE__ . $errorHandler->__(' - APIKey miss initialized')) : false;
            throw new Exception($errorHandler->__('APIKey miss initialized in APICall constructor.'));
        }

        return $this;
    }

//    public function setApiKey($apiKey) {
//        if ($apiKey == null) {
//            throw new InvalidArgumentException("Parameter APIKey can't be null for the setApiKey() function.");
//        } else {
//            if ($apiKey == "") {
//                throw new InvalidArgumentException("API Key can't be blank");
//            } else {
//                $this->_apiKey = $apiKey;
//            }
//        }
//    }
//
//    public function setBroadcasterId($broadcasterId) {
//        if ($broadcasterId != null) {
//            if (is_numeric($broadcasterId)) {
//                $this->_broadcasterId = $broadcasterId;
//            } else {
//                throw new UnexpectedValueException("Broadcaster_id parameter is not numeric for the setBroadcasterId() function.");
//            }
//        } else {
//
//            throw new UnexpectedValueException("Broadcaster_id parameter can't be null for the setBroadcasterId() function.");
//        }
//
//        $this->_broadcasterId = $broadcasterId;
//    }
//
//    public function setAction($action) {
//        if ($action != null) {
//
//            if ($action == "POST" || $action == "DELETE" || $action == "GET") {
//
//            } else {
//                throw new UnexpectedValueException("Wrong value for the setAction() function (can be POST,DELETE or GET).");
//            }
//        } else {
//
//            throw new UnexpectedValueException("Action parameter can't be null for the setAction() function.");
//        }
//
//
//        $this->_action = $action;
//    }

    function ApiRequest($action, $url)
    {
        $this->_log->logInfo("action = $action ; url= $url");
        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_CAINFO, "cacert.pem");
            curl_setopt($ch, CURLOPT_VERBOSE, 1);

            switch ($action) {
                case "POST":
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    break;
                case "GET":
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
                    break;
                case "DELETE":
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
                    break;

                default:
                    throw new Exception("Wrong value for the setAction() function (can be POST,DELETE or GET).");
                    break;
            }

            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
            curl_setopt($ch, CURLOPT_USERAGENT, "MozillaXYZ/1.0");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            $output = curl_exec($ch);

            $this->_jsonDecoded = json_decode($output, true);

            if (curl_errno($ch)) {
                return 'error:' . curl_error($ch);
            }
            curl_close($ch);
        } catch (Exception $e) {
            $this->_logError->logError(__LINE__ . " " . __FILE__ . " " . $e->getMessage());
            trigger_error($e->getMessage(), E_USER_WARNING);
        }
    }

    function ApiRequestWithRawData($url)
    {
        //use to make API call for the Raw return (like embed code)
        $this->_getLogHandler()->logInfo("GET RAW DATA url= $url");
        try {
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CAINFO, "cacert.pem");
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_USERAGENT, "MozillaXYZ/1.0");
            curl_setopt($ch, CURLOPT_VERBOSE, 1);

            $output = curl_exec($ch);

            $this->setJson($output);
            if (curl_errno($ch)) {
                throw new Exception('error:' . curl_error($ch));
                return;
            }
            curl_close($ch);
        } catch (Exception $e) {
            $this->_getLogErrorHandler()->logError(__LINE__ . " " . __FILE__ . " GET RAW DATA url = " . $url . $e->getMessage());
            trigger_error($e->getMessage(), E_USER_WARNING);
        }
    }
}