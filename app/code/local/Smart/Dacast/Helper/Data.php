<?php
/**
 * Dacast integration module
 *
 * @category    Smart
 * @package     Smart_Dacast
 * @author      Pavel Panchyshnyy ppanchyshnyy@gmail.com
 */

class Smart_Dacast_Helper_Data extends Mage_Adminhtml_Helper_Data
{
    const XML_PATH_DACAST_ENABLED = 'sm_dacast/settings/enable';
    const DACAST_URL_PATH         = 'http://www.dacast.com/';

    /**
     * @return int
     */
    protected function _getStoreId()
    {
        return Mage::app()->getStore()->getId();
    }

    /**
     * @return bool
     */
    public function isModuleEnabled()
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_DACAST_ENABLED);
    }

    /**
     * @return string
     */
    public function getDacastUrl()
    {
        return self::DACAST_URL_PATH;
    }
}